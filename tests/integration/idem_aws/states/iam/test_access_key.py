import time
from collections import ChainMap

import pytest
import pytest_asyncio


async def delete_all_keys(hub, ctx, user_name):
    keys_list = await hub.exec.aws.iam.access_key.list(ctx, user_name=user_name)
    assert keys_list["result"], keys_list["comment"]
    for key in keys_list["ret"]:
        ret = await hub.exec.aws.iam.access_key.delete(
            ctx, user_name=user_name, access_key_id=key["access_key_id"]
        )
        assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_iam_user(hub, ctx, aws_iam_user):
    yield aws_iam_user
    await delete_all_keys(hub, ctx, user_name=aws_iam_user["name"])


@pytest.mark.asyncio
async def test_present_create_inactive(hub, ctx, aws_iam_user):
    user_name = aws_iam_user["user_name"]

    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name=f"idem-test-iam-access-key-{int(time.time())}",
        user_name=user_name,
        status="Inactive",
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and ret["new_state"]
    assert "Inactive" == ret["new_state"]["status"]


@pytest.mark.asyncio
async def test_lifecycle_manual_operations(hub, ctx, aws_iam_user):
    # In this test we're verifying everything works with user_name and access_key_id
    #  A user may do an initial state run with any of these states.
    access_key_name = f"idem-test-iam-access-key-{int(time.time())}"
    user_name = aws_iam_user["name"]

    # test create
    ctx["test"] = True
    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name=access_key_name,
        status="Active",
        user_name=user_name,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"]["name"] == access_key_name
    assert ret["new_state"]["secret_access_key"] == "SECRET_ACCESS_KEY"
    assert ret["new_state"]["resource_id"] == "GENERATED_KEY"
    assert ret["new_state"]["status"] == "Active"
    ctx["test"] = False

    # actually create
    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name=access_key_name,
        status="Active",
        user_name=user_name,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"]["name"] == access_key_name
    # make sure the resource id was generated
    assert ret["new_state"]["resource_id"]
    assert ret["new_state"]["secret_access_key"]
    assert ret["new_state"]["status"] == "Active"

    access_key_id = ret["new_state"]["resource_id"]
    secret = ret["new_state"]["secret_access_key"]

    # find a key via describe
    describe_ret = await hub.states.aws.iam.access_key.describe(ctx)
    describe_key_name = f"{user_name}-{access_key_id}"
    resource_key = f"iam-access-key-{describe_key_name}"
    assert resource_key in describe_ret
    # Verify that the describe output format is correct
    assert "aws.iam.access_key.present" in describe_ret[resource_key]
    described_resource = describe_ret[resource_key].get("aws.iam.access_key.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert describe_key_name == described_resource_map.get("name")
    assert access_key_id == described_resource_map.get("resource_id")
    # Describe is not going to output secret.
    assert not described_resource_map.get("secret_access_key")
    assert "Active" == described_resource_map.get("status")

    # re-run, no changes (idempotent)
    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name=access_key_name,
        status="Active",
        user_name=user_name,
        resource_id=access_key_id,
        secret_access_key=secret,
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"]["name"] == access_key_name
    assert ret["new_state"]["resource_id"] == access_key_id
    assert ret["new_state"]["status"] == "Active"
    assert ret["new_state"]["secret_access_key"] == secret
    assert ret["old_state"] == ret["new_state"]

    # change status with --test
    ctx["test"] = True
    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name=access_key_name,
        status="Inactive",
        user_name=user_name,
        resource_id=access_key_id,
        secret_access_key=secret,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]["name"] == access_key_name
    assert ret["old_state"]["resource_id"] == access_key_id
    assert ret["old_state"]["secret_access_key"] == secret
    assert ret["old_state"]["status"] == "Active"
    assert ret["new_state"]["name"] == access_key_name
    assert ret["new_state"]["resource_id"] == access_key_id
    assert ret["new_state"]["secret_access_key"] == secret
    assert ret["new_state"]["status"] == "Inactive"
    ctx["test"] = False

    # change status
    ret = await hub.states.aws.iam.access_key.present(
        ctx,
        name=access_key_name,
        status="Inactive",
        user_name=user_name,
        resource_id=access_key_id,
        secret_access_key=secret,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]["name"] == access_key_name
    assert ret["old_state"]["resource_id"] == access_key_id
    assert ret["old_state"]["secret_access_key"] == secret
    assert ret["old_state"]["status"] == "Active"
    assert ret["new_state"]["name"] == access_key_name
    assert ret["new_state"]["resource_id"] == access_key_id
    assert ret["new_state"]["secret_access_key"] == secret
    assert ret["new_state"]["status"] == "Inactive"

    # delete
    ret = await hub.states.aws.iam.access_key.absent(
        ctx,
        name=access_key_name,
        user_name=user_name,
        resource_id=access_key_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == access_key_name
    assert ret["old_state"]["resource_id"] == access_key_id
    assert ret["old_state"]["status"] == "Inactive"
    assert "Deleted" in str(ret["comment"])

    # re-run, no changes (idempotent)
    ret = await hub.states.aws.iam.access_key.absent(
        ctx,
        name=access_key_name,
        user_name=user_name,
        resource_id=access_key_id,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert "already absent" in str(ret["comment"])
