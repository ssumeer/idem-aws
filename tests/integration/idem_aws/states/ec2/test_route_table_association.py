import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_route_table_association(
    hub, ctx, aws_ec2_subnet, aws_ec2_internet_gateway, aws_ec2_route_table
):
    route_table_association_name = "idem-test-route-table-association" + str(
        uuid.uuid4()
    )
    route_table_id = aws_ec2_route_table.get("resource_id")

    # create route table association with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.route_table_association.present(
        test_ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        gateway_id=aws_ec2_internet_gateway.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert aws_ec2_internet_gateway.get("resource_id") == resource.get("gateway_id")

    # create route table association
    ret = await hub.states.aws.ec2.route_table_association.present(
        ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        gateway_id=aws_ec2_internet_gateway.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert aws_ec2_internet_gateway.get("resource_id") == resource.get("gateway_id")

    # update route table association will result in no-op
    ret = await hub.states.aws.ec2.route_table_association.present(
        ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert aws_ec2_internet_gateway.get("resource_id") == resource.get("gateway_id")
    assert (
        f"aws.ec2.route_table_association '{route_table_association_name}' association already exists"
        in ret["comment"]
    )

    # create another route table association with subnet with --test flag
    ret = await hub.states.aws.ec2.route_table_association.present(
        test_ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert aws_ec2_subnet.get("SubnetId") == resource.get("subnet_id")

    # create another route table association with subnet
    ret = await hub.states.aws.ec2.route_table_association.present(
        ctx,
        name="test-subnet-association",
        route_table_id=route_table_id,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert aws_ec2_subnet.get("SubnetId") == resource.get("subnet_id")
    subnet_association_resource_id = resource.get("resource_id")

    # Describe route table association
    describe_ret = await hub.states.aws.ec2.route_table_association.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.route_table_association.present"
    )
    described_resource = describe_ret.get(resource_id).get(
        "aws.ec2.route_table_association.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert route_table_id == described_resource_map.get("route_table_id")

    # delete route table association with test flag
    ret = await hub.states.aws.ec2.route_table_association.absent(
        test_ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.ec2.route_table_association",
            name=route_table_association_name,
        )[0]
        in ret["comment"]
    )

    ret = await hub.states.aws.ec2.route_table_association.absent(
        ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ec2.route_table_association",
            name=route_table_association_name,
        )[0]
        in ret["comment"]
    )

    # delete the same resource again
    ret = await hub.states.aws.ec2.route_table_association.absent(
        ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.route_table_association",
            name=route_table_association_name,
        )[0]
        in ret["comment"]
    )

    # delete without passing resource_id
    ret = await hub.states.aws.ec2.route_table_association.absent(
        ctx,
        name=route_table_association_name,
        route_table_id=route_table_id,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.route_table_association",
            name=route_table_association_name,
        )[0]
        in ret["comment"]
    )

    # delete another route table association resource
    ret = await hub.states.aws.ec2.route_table_association.absent(
        ctx,
        name="test-subnet-association",
        route_table_id=route_table_id,
        resource_id=subnet_association_resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ec2.route_table_association",
            name="test-subnet-association",
        )[0]
        in ret["comment"]
    )
