import copy
import time
from collections import ChainMap
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

PARAMETER = {
    "name": "idem-test-vpc-peering-connection-options-" + str(int(time.time())),
}


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_vpc_peering_connection(
    hub, ctx, aws_ec2_vpc, aws_ec2_vpc_2
) -> Dict[str, Any]:
    """
    Create and cleanup an ec2 vpc peering connection for a module that needs it
    :return: a description of an ec2 vpc peering connection
    """
    global PARAMETER

    vpc_peering_connection_temp_name = "idem-fixture-vpc-peering-connection" + str(
        int(time.time())
    )
    PARAMETER["name"] = vpc_peering_connection_temp_name
    ret = await hub.states.aws.ec2.vpc_peering_connection.present(
        ctx,
        name=vpc_peering_connection_temp_name,
        vpc_id=aws_ec2_vpc_2["VpcId"],
        peer_vpc_id=aws_ec2_vpc["VpcId"],
        peer_owner_id=aws_ec2_vpc["OwnerId"],
        peer_region=ctx["acct"].get("region_name"),
        status="active",
        tags={"idem-test-key-name": vpc_peering_connection_temp_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")

    resource = hub.tool.boto3.resource.create(
        ctx, "ec2", "VpcPeeringConnection", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc_peering_connection.absent(
        ctx,
        name=vpc_peering_connection_temp_name,
        resource_id=after["VpcPeeringConnectionId"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_ec2_vpc_peering_connection, __test):
    # Test update with and without a flag
    global PARAMETER
    ctx["test"] = __test

    # Set up initial values
    PARAMETER["peer_allow_remote_vpc_dns_resolution"] = aws_ec2_vpc_peering_connection[
        "AccepterVpcInfo"
    ]["PeeringOptions"]["AllowDnsResolutionFromRemoteVpc"]
    PARAMETER["peer_allow_classic_link_to_remote_vpc"] = aws_ec2_vpc_peering_connection[
        "AccepterVpcInfo"
    ]["PeeringOptions"]["AllowEgressFromLocalClassicLinkToRemoteVpc"]
    PARAMETER["peer_allow_vpc_to_remote_classic_link"] = aws_ec2_vpc_peering_connection[
        "AccepterVpcInfo"
    ]["PeeringOptions"]["AllowEgressFromLocalVpcToRemoteClassicLink"]
    PARAMETER["allow_remote_vpc_dns_resolution"] = aws_ec2_vpc_peering_connection[
        "RequesterVpcInfo"
    ]["PeeringOptions"]["AllowDnsResolutionFromRemoteVpc"]
    PARAMETER["allow_classic_link_to_remote_vpc"] = aws_ec2_vpc_peering_connection[
        "RequesterVpcInfo"
    ]["PeeringOptions"]["AllowEgressFromLocalClassicLinkToRemoteVpc"]
    PARAMETER["allow_vpc_to_remote_classic_link"] = aws_ec2_vpc_peering_connection[
        "RequesterVpcInfo"
    ]["PeeringOptions"]["AllowEgressFromLocalVpcToRemoteClassicLink"]
    PARAMETER["resource_id"] = aws_ec2_vpc_peering_connection["VpcPeeringConnectionId"]

    new_parameter = copy.deepcopy(PARAMETER)

    # Update
    new_parameter["peer_allow_remote_vpc_dns_resolution"] = True
    new_parameter["allow_classic_link_to_remote_vpc"] = True

    ret = await hub.states.aws.ec2.vpc_peering_connection_options.present(
        ctx,
        **new_parameter,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_resource_options_comment(
                "aws.ec2.vpc_peering_connection_options",
                PARAMETER["resource_id"],
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.ec2.vpc_peering_connection_options", PARAMETER["name"]
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_resource_options_comment(
                "aws.ec2.vpc_peering_connection_options",
                PARAMETER["resource_id"],
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_comment(
                "aws.ec2.vpc_peering_connection_options", PARAMETER["name"]
            )
            == ret["comment"]
        )

    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"]

    assert (
        ret["old_state"]["peer_allow_remote_vpc_dns_resolution"]
        == PARAMETER["peer_allow_remote_vpc_dns_resolution"]
    )
    assert (
        ret["old_state"]["peer_allow_classic_link_to_remote_vpc"]
        == PARAMETER["peer_allow_classic_link_to_remote_vpc"]
    )
    assert (
        ret["old_state"]["peer_allow_vpc_to_remote_classic_link"]
        == PARAMETER["peer_allow_vpc_to_remote_classic_link"]
    )
    assert (
        ret["old_state"]["allow_remote_vpc_dns_resolution"]
        == PARAMETER["allow_remote_vpc_dns_resolution"]
    )
    assert (
        ret["old_state"]["allow_classic_link_to_remote_vpc"]
        == PARAMETER["allow_classic_link_to_remote_vpc"]
    )
    assert (
        ret["old_state"]["allow_vpc_to_remote_classic_link"]
        == PARAMETER["allow_vpc_to_remote_classic_link"]
    )

    assert (
        ret["new_state"]["peer_allow_remote_vpc_dns_resolution"]
        == new_parameter["peer_allow_remote_vpc_dns_resolution"]
    )
    assert (
        ret["new_state"]["peer_allow_classic_link_to_remote_vpc"]
        == new_parameter["peer_allow_classic_link_to_remote_vpc"]
    )
    assert (
        ret["new_state"]["peer_allow_vpc_to_remote_classic_link"]
        == new_parameter["peer_allow_vpc_to_remote_classic_link"]
    )
    assert (
        ret["new_state"]["allow_remote_vpc_dns_resolution"]
        == new_parameter["allow_remote_vpc_dns_resolution"]
    )
    assert (
        ret["new_state"]["allow_classic_link_to_remote_vpc"]
        == new_parameter["allow_classic_link_to_remote_vpc"]
    )
    assert (
        ret["new_state"]["allow_vpc_to_remote_classic_link"]
        == new_parameter["allow_vpc_to_remote_classic_link"]
    )

    assert (
        ret["new_state"]["peer_allow_remote_vpc_dns_resolution"]
        != ret["old_state"]["peer_allow_remote_vpc_dns_resolution"]
    )
    assert (
        ret["new_state"]["allow_classic_link_to_remote_vpc"]
        != ret["old_state"]["allow_classic_link_to_remote_vpc"]
    )

    assert (
        ret["new_state"]["peer_allow_classic_link_to_remote_vpc"]
        == ret["old_state"]["peer_allow_classic_link_to_remote_vpc"]
    )
    assert (
        ret["new_state"]["peer_allow_vpc_to_remote_classic_link"]
        == ret["old_state"]["peer_allow_vpc_to_remote_classic_link"]
    )
    assert (
        ret["new_state"]["allow_remote_vpc_dns_resolution"]
        == ret["old_state"]["allow_remote_vpc_dns_resolution"]
    )
    assert (
        ret["new_state"]["allow_vpc_to_remote_classic_link"]
        == ret["old_state"]["allow_vpc_to_remote_classic_link"]
    )

    assert ret["old_state"]["resource_id"] == PARAMETER["resource_id"]

    PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # Test describe
    describe_ret = await hub.states.aws.ec2.vpc_peering_connection_options.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret

    # Verify that the describe function output format is correct
    assert "aws.ec2.vpc_peering_connection_options.present" in describe_ret[resource_id]

    described_resource = describe_ret[resource_id].get(
        "aws.ec2.vpc_peering_connection_options.present"
    )

    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER[
        "peer_allow_remote_vpc_dns_resolution"
    ] == described_resource_map.get("peer_allow_remote_vpc_dns_resolution")
    assert PARAMETER[
        "peer_allow_classic_link_to_remote_vpc"
    ] == described_resource_map.get("peer_allow_classic_link_to_remote_vpc")
    assert PARAMETER[
        "peer_allow_vpc_to_remote_classic_link"
    ] == described_resource_map.get("peer_allow_vpc_to_remote_classic_link")
    assert PARAMETER["allow_remote_vpc_dns_resolution"] == described_resource_map.get(
        "allow_remote_vpc_dns_resolution"
    )
    assert PARAMETER["allow_classic_link_to_remote_vpc"] == described_resource_map.get(
        "allow_classic_link_to_remote_vpc"
    )
    assert PARAMETER["allow_vpc_to_remote_classic_link"] == described_resource_map.get(
        "allow_vpc_to_remote_classic_link"
    )
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    # Test delete with and without a flag. The behavior should be the same either way
    ctx["test"] = __test

    ret = await hub.states.aws.ec2.vpc_peering_connection_options.absent(
        ctx, name=PARAMETER["name"]
    )

    assert ret["result"], ret["comment"]
