import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-v1-lb-" + str(int(time.time())),
    "scheme": "internet-facing",
}
RESOURCE_TYPE = "aws.elb.load_balancer"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    cleanup,
    aws_ec2_default_vpc,
    aws_ec2_default_subnets,
    aws_ec2_default_internet_gateway,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test

    assert (
        aws_ec2_default_vpc
        and aws_ec2_default_subnets
        and aws_ec2_default_internet_gateway
    )
    assert aws_ec2_default_vpc.get(
        "default_vpc_id"
    ), f"Test needs default VPC. Default VPC is not found in region : {ctx['acct'].get('region_name')}"
    assert aws_ec2_default_subnets.get(
        "id_a_subnet"
    ), f"Default subnet not found in subregion: {ctx['acct'].get('region_name') + 'a'}"
    assert aws_ec2_default_subnets.get(
        "id_b_subnet"
    ), f"Default subnet not found in subregion: {ctx['acct'].get('region_name') + 'b'}"
    assert aws_ec2_default_subnets.get(
        "id_c_subnet"
    ), f"Default subnet not found in subregion: {ctx['acct'].get('region_name') + 'c'}"
    assert aws_ec2_default_internet_gateway.get(
        "internet_gateway_id"
    ), f"Default VPC needs to have an attached Internet Gateway."

    load_balancer_attributes = (
        {
            "CrossZoneLoadBalancing": {
                "Enabled": True,
            },
        },
    )
    PARAMETER["default_vpc_id"] = aws_ec2_default_vpc.get("default_vpc_id")
    PARAMETER["id_a_subnet"] = aws_ec2_default_subnets.get("id_a_subnet")
    PARAMETER["id_b_subnet"] = aws_ec2_default_subnets.get("id_b_subnet")
    PARAMETER["id_c_subnet"] = aws_ec2_default_subnets.get("id_c_subnet")
    PARAMETER["internet_gateway_id"] = aws_ec2_default_internet_gateway.get(
        "internet_gateway_id"
    )
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    PARAMETER["attributes"] = load_balancer_attributes
    PARAMETER["subnets"] = [
        PARAMETER["id_a_subnet"],
        PARAMETER["id_b_subnet"],
    ]
    PARAMETER["availability_zones"] = [
        ctx["acct"].get("region_name") + "a",
        ctx["acct"].get("region_name") + "b",
    ]
    PARAMETER["listeners"] = [
        {
            "InstancePort": 80,
            "InstanceProtocol": "HTTP",
            "LoadBalancerPort": 80,
            "Protocol": "HTTP",
        },
    ]
    # Create ElasticLoadBalancing load_balancer
    response = await hub.states.aws.elb.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        listeners=PARAMETER["listeners"],
        availability_zones=PARAMETER["availability_zones"],
        tags=PARAMETER["tags"],
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["availability_zones"] and len(PARAMETER["availability_zones"]) == 2
    assert set(PARAMETER["availability_zones"]) == set(
        resource.get("availability_zones")
    )
    assert resource.get("listeners")
    assert len(PARAMETER["listeners"]) == len(resource.get("listeners"))
    assert PARAMETER["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get_load_balancer(hub, ctx):
    # This test is here to avoid creating another ElasticLoadBalancing Load Balancer fixture for exec.get() testing.
    ret = await hub.exec.aws.elb.load_balancer.get(
        ctx,
        resource_id=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["availability_zones"] and len(PARAMETER["availability_zones"]) == 2
    assert set(PARAMETER["availability_zones"]) == set(
        resource.get("availability_zones")
    )
    assert resource.get("listeners")
    assert len(PARAMETER["listeners"]) == len(resource.get("listeners"))


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_name(hub, ctx):
    v1_lb_name = "idem-test-exec-v1-lb-" + str(int(time.time()))
    ret = await hub.exec.aws.elb.load_balancer.get(
        ctx,
        resource_id=v1_lb_name,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.elb.load_balancer '{v1_lb_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-subnets", depends=["present"])
async def test_update_subnets_and_zones(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    PARAMETER["availability_zones"] = [
        ctx["acct"].get("region_name") + "b",
        ctx["acct"].get("region_name") + "c",
    ]
    response = await hub.states.aws.elb.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        listeners=PARAMETER["listeners"],
        availability_zones=PARAMETER["availability_zones"],
        resource_id=PARAMETER["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )

    resource = response.get("new_state")
    assert len(PARAMETER["availability_zones"]) == len(
        resource.get("availability_zones")
    )
    assert set(PARAMETER["availability_zones"]) == set(
        resource.get("availability_zones")
    )
    assert resource.get("listeners")
    assert len(PARAMETER["listeners"]) == len(resource.get("listeners"))
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")

    # Update ElasticLoadBalancing load_balancer. Update subnets.
    response = await hub.states.aws.elb.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        listeners=PARAMETER["listeners"],
        subnets=PARAMETER["subnets"],
        resource_id=PARAMETER["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert len(resource.get("subnets")) == 2
    assert set(PARAMETER["subnets"]) == set(resource.get("subnets"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-attributes", depends=["update-subnets"])
async def test_update_attributes_and_listeners(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    # Update ElasticLoadBalancing load_balancer. Update listeners and security_groups.
    PARAMETER["listeners"] = [
        {
            "InstancePort": 80,
            "InstanceProtocol": "HTTP",
            "LoadBalancerPort": 80,
            "Protocol": "HTTP",
        },
        {
            "InstancePort": 8000,
            "InstanceProtocol": "TCP",
            "LoadBalancerPort": 8000,
            "Protocol": "TCP",
        },
    ]
    PARAMETER["load_balancer_attributes"] = {
        "CrossZoneLoadBalancing": {
            "Enabled": True,
        },
        "ConnectionSettings": {
            "IdleTimeout": 60,
        },
        "ConnectionDraining": {"Enabled": True, "Timeout": 123},
    }
    response = await hub.states.aws.elb.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        listeners=PARAMETER["listeners"],
        availability_zones=PARAMETER["availability_zones"],
        load_balancer_attributes=PARAMETER["load_balancer_attributes"],
        resource_id=PARAMETER["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )

    resource = response.get("new_state")
    assert len(PARAMETER["availability_zones"]) == len(
        resource.get("availability_zones")
    )
    assert set(PARAMETER["availability_zones"]) == set(
        resource.get("availability_zones")
    )
    assert resource.get("listeners")
    assert len(PARAMETER["listeners"]) == len(resource.get("listeners"))
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("load_balancer_attributes")
    assert (
        PARAMETER["load_balancer_attributes"].get("CrossZoneLoadBalancing")
        == resource.get("load_balancer_attributes")["CrossZoneLoadBalancing"]
    )
    assert (
        PARAMETER["load_balancer_attributes"].get("ConnectionSettings")
        == resource.get("load_balancer_attributes")["ConnectionSettings"]
    )
    assert (
        PARAMETER["load_balancer_attributes"].get("ConnectionDraining")
        == resource.get("load_balancer_attributes")["ConnectionDraining"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-tags", depends=["update-attributes"])
async def test_update_tags(hub, ctx, __test):
    # Update ElasticLoadBalancing load_balancer.Update tags.
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"], "Description": "ELB Load Balancer."}
    response = await hub.states.aws.elb.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        listeners=PARAMETER["listeners"],
        load_balancer_attributes=PARAMETER["load_balancer_attributes"],
        resource_id=PARAMETER["resource_id"],
        tags=PARAMETER["tags"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )

    resource = response.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")

    PARAMETER["tags"] = {
        "Description": "ELB Load Balancer. Updated",
        "Summary": "Summary of ELB Load Balancer",
    }
    response = await hub.states.aws.elb.load_balancer.present(
        ctx,
        name=PARAMETER["name"],
        listeners=PARAMETER["listeners"],
        resource_id=PARAMETER["resource_id"],
        tags=PARAMETER["tags"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["update-tags"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.aws.elb.load_balancer.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("aws.elb.load_balancer.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "aws.elb.load_balancer.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert (
        PARAMETER["load_balancer_attributes"].get("CrossZoneLoadBalancing")
        == resource.get("load_balancer_attributes")["CrossZoneLoadBalancing"]
    )
    assert (
        PARAMETER["load_balancer_attributes"].get("ConnectionSettings")
        == resource.get("load_balancer_attributes")["ConnectionSettings"]
    )
    assert (
        PARAMETER["load_balancer_attributes"].get("ConnectionDraining")
        == resource.get("load_balancer_attributes")["ConnectionDraining"]
    )
    assert len(PARAMETER["availability_zones"]) == len(
        resource.get("availability_zones")
    )
    assert set(PARAMETER["availability_zones"]) == set(
        resource.get("availability_zones")
    )
    assert resource.get("listeners")
    assert len(PARAMETER["listeners"]) == len(resource.get("listeners"))
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elb.load_balancer.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    describe_resource = ret.get("old_state")
    resource = dict(ChainMap(describe_resource))
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    assert len(PARAMETER["availability_zones"]) == len(
        resource.get("availability_zones")
    )
    assert set(PARAMETER["availability_zones"]) == set(
        resource.get("availability_zones")
    )
    assert resource.get("listeners")
    assert len(PARAMETER["listeners"]) == len(resource.get("listeners"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already-absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elb.load_balancer.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_absent_with_none_resource_id(hub, ctx):
    lb_temp_name = "idem-test-elbv2-lb" + str(int(time.time()))
    # Delete ELBv2 LB with resource_id as None. Result in no-op.
    ret = await hub.states.aws.elb.load_balancer.absent(ctx, name=lb_temp_name)
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=lb_temp_name
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_absent_with_invalid_resource_id(hub, ctx):
    ret = await hub.states.aws.elb.load_balancer.absent(
        ctx,
        name="invalid-xyz-abc",
        resource_id="invalid-xyz-abc",
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name="invalid-xyz-abc"
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.elb.load_balancer.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
