import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_notification(hub, ctx, aws_s3_bucket_policy, aws_lambda_function):
    function_arn = aws_lambda_function["function_arn"]
    bucket_name = aws_s3_bucket_policy.get("bucket")
    permission_statement_id = str(uuid.uuid4())
    # Add a resource policy to allow S3 permission to invoke your function
    add_permission = await hub.exec.boto3.client["lambda"].add_permission(
        ctx,
        FunctionName=aws_lambda_function["name"],
        StatementId=permission_statement_id,
        Action="lambda:InvokeFunction",
        Principal="s3.amazonaws.com",
        SourceArn=f"arn:aws:s3:::{bucket_name}",
    )

    assert add_permission["result"]

    # Create bucket notifications with two configuration

    notifications = {
        "LambdaFunctionConfigurations": [
            {
                "Id": "test",
                "LambdaFunctionArn": function_arn,
                "Events": ["s3:ObjectCreated:*"],
                "Filter": {
                    "Key": {
                        "FilterRules": [
                            {"Name": "Prefix", "Value": ""},
                            {"Name": "Suffix", "Value": ".jpeg"},
                        ]
                    }
                },
            },
            {
                "Id": "test2",
                "LambdaFunctionArn": function_arn,
                "Events": ["s3:ObjectCreated:*"],
                "Filter": {
                    "Key": {
                        "FilterRules": [
                            {"Name": "Prefix", "Value": ""},
                            {"Name": "Suffix", "Value": ".png"},
                        ]
                    }
                },
            },
        ]
    }
    # create test context
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # create bucket notification in test
    ret = await hub.states.aws.s3.bucket_notification.present(
        test_ctx,
        name=bucket_name,
        notifications=notifications,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.s3.bucket_notification", name=bucket_name
        )[0]
        in ret["comment"]
    )

    assert not ret.get("old_state") and ret.get("new_state")

    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("notifications").get("LambdaFunctionConfigurations"),
        notifications.get("LambdaFunctionConfigurations"),
    )
    assert resource["name"] == bucket_name
    assert resource["resource_id"] == bucket_name + "-notifications"

    # create in real
    ret = await hub.states.aws.s3.bucket_notification.present(
        ctx,
        name=bucket_name,
        notifications=notifications,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        resource.get("notifications").get("LambdaFunctionConfigurations"),
        notifications.get("LambdaFunctionConfigurations"),
    )
    assert resource["name"] == bucket_name
    assert resource["resource_id"] == bucket_name + "-notifications"

    # update in test
    ret = await hub.states.aws.s3.bucket_notification.present(
        test_ctx,
        name=bucket_name,
        resource_id=bucket_name + "-notifications",
        notifications={
            "LambdaFunctionConfigurations": [
                {
                    "Id": "test2",
                    "LambdaFunctionArn": function_arn,
                    "Events": ["s3:ObjectCreated:*"],
                    "Filter": {
                        "Key": {
                            "FilterRules": [
                                {"Name": "Prefix", "Value": ""},
                                {"Name": "Suffix", "Value": ".png"},
                            ]
                        }
                    },
                }
            ]
        },
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_update_comment(
            resource_type="aws.s3.bucket_notification", name=bucket_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        len(
            ret.get("new_state")
            .get("notifications")
            .get("LambdaFunctionConfigurations")
        )
        == 1
    )

    # update in real
    ret = await hub.states.aws.s3.bucket_notification.present(
        ctx,
        name=bucket_name,
        resource_id=bucket_name + "-notifications",
        notifications={
            "LambdaFunctionConfigurations": [
                {
                    "Id": "test2",
                    "LambdaFunctionArn": function_arn,
                    "Events": ["s3:ObjectCreated:*"],
                    "Filter": {
                        "Key": {
                            "FilterRules": [
                                {"Name": "Prefix", "Value": ""},
                                {"Name": "Suffix", "Value": ".png"},
                            ]
                        }
                    },
                }
            ]
        },
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        len(
            ret.get("new_state")
            .get("notifications")
            .get("LambdaFunctionConfigurations")
        )
        == 1
    )

    # Describe s3 bucket notifications
    describe_ret = await hub.states.aws.s3.bucket_notification.describe(ctx)
    # Verify that describe output format is correct
    assert "aws.s3.bucket_notification.present" in describe_ret.get(
        bucket_name + "-notifications"
    )
    describe_params = describe_ret.get(bucket_name + "-notifications").get(
        "aws.s3.bucket_notification.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert "notifications" in described_resource_map
    assert "name" in described_resource_map
    assert "resource_id" in described_resource_map

    # remove all notification configuration
    ret = await hub.states.aws.s3.bucket_notification.absent(
        ctx,
        name=bucket_name,
        resource_id=bucket_name + "-notifications",
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # remove again
    ret = await hub.states.aws.s3.bucket_notification.absent(
        ctx,
        name=bucket_name,
        resource_id=bucket_name + "-notifications",
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")

    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket_notification", name=bucket_name
        )[0]
        in ret["comment"]
    )

    # Remove the resource policy to allow S3 permission to invoke your function

    remove_permission = await hub.exec.boto3.client["lambda"].remove_permission(
        ctx,
        FunctionName=aws_lambda_function["name"],
        StatementId=permission_statement_id,
    )

    assert remove_permission["result"]
