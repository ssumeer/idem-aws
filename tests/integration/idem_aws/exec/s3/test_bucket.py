import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_s3_bucket):
    s3_bucket_get_name = "idem-test-exec-get-s3-bucket-" + str(int(time.time()))
    ret = await hub.exec.aws.s3.bucket.get(
        ctx,
        name=s3_bucket_get_name,
        resource_id=aws_s3_bucket["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_s3_bucket["resource_id"] == resource["ret"].get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_s3_bucket):
    s3_bucket_list_name = "idem-test-exec-list-s3-bucket-" + str(int(time.time()))
    ret = await hub.exec.aws.s3.bucket.list(ctx, name=s3_bucket_list_name)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    bucket_list = []
    for bucket in ret["ret"]:
        bucket_list.append(bucket["ret"].get("name"))
    if aws_s3_bucket["resource_id"] not in bucket_list:
        assert False
    else:
        assert True
