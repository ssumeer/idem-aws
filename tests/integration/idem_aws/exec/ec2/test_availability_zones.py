from typing import Any
from typing import Dict
from typing import List

import pytest
import pytest_asyncio


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, aws_availability_zones):
    az = aws_availability_zones[0]
    ret = await hub.exec.aws.ec2.availability_zones.get(ctx, name=az["zone_name"])
    assert ret["result"], ret["comment"]
    # localstack does not return all az attributes
    if hub.tool.utils.is_running_localstack(ctx):
        assert az["resource_id"] == ret["ret"]["resource_id"]
        assert az["region_name"] == ret["ret"]["region_name"]
        assert az["state"] == ret["ret"]["state"]
        assert az["zone_name"] == ret["ret"]["zone_name"]
    else:
        assert az == ret["ret"]


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, aws_availability_zones):
    az = aws_availability_zones[0]
    ret = await hub.exec.aws.ec2.availability_zones.get(
        ctx, resource_id=az["resource_id"]
    )
    assert ret["result"], ret["comment"]
    # localstack does not return all az attributes
    if hub.tool.utils.is_running_localstack(ctx):
        assert az["resource_id"] == ret["ret"]["resource_id"]
        assert az["region_name"] == ret["ret"]["region_name"]
        assert az["state"] == ret["ret"]["state"]
        assert az["zone_name"] == ret["ret"]["zone_name"]
    else:
        assert az == ret["ret"]


@pytest.mark.asyncio
async def test_get_more_than_one(hub, ctx):
    ret = await hub.exec.aws.ec2.availability_zones.get(
        ctx,
    )
    assert ret["result"]
    assert (
        "More than one aws.ec2.availability_zones resource was found. Use resource"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)  # localstack always return a list of azs
async def test_get_invalid_name(hub, ctx):
    ret = await hub.exec.aws.ec2.availability_zones.get(
        ctx,
        name="fake-availability-zone-name",
    )
    assert not ret["result"]
    assert "Invalid availability zone" in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(False)  # localstack always return a list of azs
async def test_get_invalid_resource_id(hub, ctx):
    ret = await hub.exec.aws.ec2.availability_zones.get(
        ctx,
        resource_id="fake-availability-zone-id",
    )
    assert not ret["result"]
    assert "Invalid availability zone-id" in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(False)  # localstack always return a list of azs
async def test_get_empty(hub, ctx):
    filters = [{"name": "zone-type", "values": ["local-zone"]}]
    ret = await hub.exec.aws.ec2.availability_zones.get(
        ctx,
        filters=filters,
    )
    assert ret["result"]
    assert hub.tool.aws.comment_utils.list_empty_comment(
        resource_type="aws.ec2.availability_zones",
        name=None,
    ) in str(ret["comment"])


@pytest.mark.asyncio
async def test_list(hub, ctx):
    ret = await hub.exec.aws.ec2.availability_zones.list(
        ctx,
        all_availability_zones=True,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)


@pytest.mark.asyncio
async def test_list_filters(hub, ctx):
    filters = [{"name": "state", "values": ["available"]}]
    ret = await hub.exec.aws.ec2.availability_zones.list(
        ctx,
        filters=filters,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    for az in ret["ret"]:
        assert az["state"] == "available"


@pytest.mark.asyncio
@pytest.mark.localstack(False)  # localstack always return a list of azs
async def test_list_invalid_filter(hub, ctx):
    filters = [{"name": "region", "values": ["invalid"]}]
    ret = await hub.exec.aws.ec2.availability_zones.list(
        ctx,
        filters=filters,
    )
    assert not ret["result"]
    assert "The filter 'region' is invalid" in str(ret["comment"])


@pytest_asyncio.fixture(scope="module")
async def aws_availability_zones(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.aws.ec2.availability_zones.list(ctx)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)

    yield ret["ret"]
