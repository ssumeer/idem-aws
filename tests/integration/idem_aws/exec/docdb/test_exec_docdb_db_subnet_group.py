from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_list(hub, ctx, aws_docdb_subnet_group):
    list_ret = await hub.exec.aws.docdb.db_subnet_group.list(
        ctx, DBSubnetGroupName=aws_docdb_subnet_group["name"]
    )
    assert list_ret and list_ret["result"] and list_ret["ret"]
    assert isinstance(list_ret["ret"], List)
    assert len(list_ret["ret"]) == 1
    result = list_ret["ret"][0]
    assert result["name"] == aws_docdb_subnet_group["name"]
    assert (
        result["db_subnet_group_description"]
        == aws_docdb_subnet_group["db_subnet_group_description"]
    )
