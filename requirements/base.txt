boto3>=1.23.10,<1.24.0
configparser>=5.0.0,<6.0.0
idem>=18.10.3,<20.0.0
pgpy>=0.5.4,<0.6.0
deepdiff>=5.8.0,<6.0.0
heist>=6.1.0,<7.0.0
pycryptodomex>=3.10,<3.14.0
pop-serial>=1.1.0,<2.0.0
